import React, { Component } from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//import ForecastExtendedContainer from './containers/ForecastExtendedContainer';
import ForecastExtended from './components/ForecastExtended'
import LocationListContainer from './containers/LocationsListContainer';

import './App.css';

const cities = [
  'Xalapa,mx',
  'London,uk',
  'Bogota,co',
  'Ciudad de México,mx',
  'Madrid,es',
  'Buenos Aires,ar'
];
  
class App extends Component {
  constructor(){
    super();
    this.state = {
      city:null
    };
  }

  handleSelectedLocation = city => {
    this.setState({ city });
  }

  render() {
    const { city } = this.state;
    return (
      <MuiThemeProvider>
        <Grid>
          <Row>
            <Col xs={12} >
              <AppBar title="weather app" />
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={6}>
              <LocationListContainer cities={cities}>
                </LocationListContainer>
            </Col>
            <Col xs={12} md={6}>
              <Paper zDepth={4}>
                <div className='detail'>
                  { 
                    city &&
                      <ForecastExtended city={city}> 
                        </ForecastExtended>
                  }
                </div>
              </Paper>
            </Col>
          </Row>
        </Grid>
      </MuiThemeProvider>
    );
  }
}

export default App;