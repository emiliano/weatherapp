import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';
import transformForecast from './../services/transformForecast';

import './styles.css';

const api_key = '4e47c9e7cbdb04f0e5ca7410232f15c9';
const url = 'http://api.openweathermap.org/data/2.5/forecast';

class ForecastExtended extends Component{

    constructor(){
        super();
        this.state = { forecastData: null }
    }

    updateCity = city => {
        const url_forecast = `${url}?q=${city}&appid=${api_key}`;
        fetch(url_forecast)
            .then(
                data => (data.json())
            )
            .then(
                weather_data => {
                    const forecastData = transformForecast(weather_data);
                    this.setState( { forecastData });
                }
            )
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.city !== this.props.city){
            this.setState({forecastData : null});
            this.updateCity(nextProps.city);
        }
    }

    componentDidMount(){
       this.updateCity(this.props.city);
    }

    renderForecastItemDays(forecastData){
        return forecastData.map(forecast => (
            <ForecastItem 
                key={`${forecast.weekDay}${forecast.hour}`}
                weekDay={forecast.weekDay} 
                hour={forecast.hour} 
                data={forecast.data}></ForecastItem>
        ))
    }

    renderProgress(){
        return <h3>cargando el prónostico</h3>;
    }

    render(){
        const {city} = this.props;
        const { forecastData } = this.state;
        return (
            <div>
                <h2 className='forecast-title'>Pronóstico extendido para {city}</h2>
                { forecastData ? 
                    this.renderForecastItemDays(forecastData) :
                    this.renderProgress() }
            </div>
        )
    }
}

ForecastExtended.propTypes = {
    city:PropTypes.string.isRequired
}
export default ForecastExtended;