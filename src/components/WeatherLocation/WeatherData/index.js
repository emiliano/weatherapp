import React from 'react';
import WeatherTempeture from './WeatherTempeture';
import WeatherExtraInfo from './WeatherExtraInfo';

import PropTypes from 'prop-types';

import './styles.css';

const WeatherData = ({data}) => { 
    const {tempeture, weatherState, humidity, wind} = data;
    return( 
        <div className='weatherDataCont'>
            <WeatherTempeture tempeture={tempeture} weatherState={weatherState} />
            <WeatherExtraInfo humidity={humidity} wind={wind} />
        </div>);
}
WeatherData.propTypes ={
    data: PropTypes.shape({
        tempeture: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired
    })
}
export default WeatherData;