import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';

import { CLOUD, CLOUDY, SUN, RAIN, WINDY, THUNDER, DRIZZLE, SNOW } 
    from '../../../constants/weathers';

import './styles.css';

const stateToIconName = weatherState =>{
    switch(weatherState){
        case CLOUD: 
            return "cloud"
        case CLOUDY: 
            return "cloudy"
        case SUN: 
            return "day-sunny"
        case RAIN: 
            return "rain"
        case SNOW:
            return "snow"
        case WINDY: 
            return "windy"
        case THUNDER: 
            return "day-thunderstorm"
        case DRIZZLE: 
            return "day-showers"
        default:
            return "day-sunny";
    }
}

const getWeatherIcon = weatherState => {
    return (<WeatherIcons className='wicon' name={stateToIconName(weatherState)} size="4x" />)
}

const WeatherTempeture = ({tempeture, weatherState}) => (
    <div className='weatherTempetureCont'>
        {getWeatherIcon(weatherState)}
        <span className='tempeture'>{` ${tempeture}`}</span>
        <span className='tempeturetype'>Cº</span>
    </div>
);

WeatherTempeture.propTypes = {
    tempeture: PropTypes.number.isRequired,
    weatherState: PropTypes.string
}

export default WeatherTempeture;