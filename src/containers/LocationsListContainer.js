import React, { Component } from 'react'
import PropTypes from 'prop-types'
import LocationList from './../components/LocationList'
import { connect } from 'react-redux'
import { setCity } from './../actions'

class LocationsListContainer extends Component {
    handleSelectedLocation = city => {
        this.props.setCity(city);
    }

    render() {
        return (
        <LocationList cities={this.props.cities}
            onSelectedLocation={this.handleSelectedLocation}>
            </LocationList>
        )
    }
}

LocationsListContainer.propTypes = {
    setCity: PropTypes.func.isRequired,
    cities:PropTypes.array.isRequired,
}
  
const mapDispatchToPropsActions = dispacth => ({
    setCity:value => dispacth(setCity(value))
});

export default connect(null, mapDispatchToPropsActions)(LocationsListContainer);

